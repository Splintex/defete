'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    uglify = require('gulp-uglify'),
    postcss = require('gulp-postcss'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('autoprefixer'),
    spritesmith  = require('gulp.spritesmith'),
    notify = require('gulp-notify'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    cssnext = require('cssnext'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    iconfont = require("gulp-iconfont"),
    consolidate = require("gulp-consolidate"),
    reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        jsLibs: 'build/js/partials',
        css: 'build/css/',
        img: 'build/img/',
        svg: 'build/img/svg',
        video: 'build/video',
        icons: 'build/img/icons',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/**/*.js',
        sass: ['src/sass/screen.sass', 'src/sass/pace.scss'],
        img: 'src/img/**/*.*',
        video: 'src/video/**/*.*',
        svg: 'src/svg/*.svg',
        icons: 'src/img/icons/*.png',
        fonts: 'src/fonts/**/*.*',
        fontSvg: 'src/font-svg/*.svg'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        sass: 'src/sass/**/*.*',
        img: 'src/img/**/*.*',
        video: 'src/video/**/*.*',
        svg: 'src/svg/*.svg',
        icons: 'src/img/icons/*.png',
        fonts: 'src/fonts/**/*.*',
        fontSvg: 'src/font-svg/*.svg'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: false,
    host: 'localhost',
    port: 3000,
    logPrefix: "Frontend_Devil"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html) 
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) 
        .pipe(rigger()) 
        //.pipe(sourcemaps.init()) 
        //.pipe(uglify()) 
        //.pipe(sourcemaps.wborite()) 
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('sass:build', function() {

    var processors = [
        autoprefixer({browsers: ['> 1%', 'last 3 versions', 'Opera 12.1', 'IE 10'], cascade: false})
        // mqpacker({
        //     sort: function (a, b) {
        //         a = a.replace(/\D/g,'');
        //         b = b.replace(/\D/g,'');
        //         return b-a;
        //         // replace this with a-b for Mobile First approach
        //     }
        // })
    ];

    return sass(path.src.sass, {
        sourcemap: false,
        style: 'compressed',
        emitCompileError: true
    })
    .on('error', notify.onError({
        title: 'Sass Error!',
        message: '<%= error.message %>'
    }))
    .pipe(postcss(processors))
    //.pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('sprite:build', function() {
    var spriteData = 
        gulp.src(path.src.icons)
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: '_sprite.sass',
                cssFormat: 'sass',
                algorithm: 'binary-tree',
                cssTemplate: 'sass.template.mustache',
                cssVarMap: function(sprite) {
                    sprite.name = sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest('build/img/'));
    spriteData.css.pipe(gulp.dest('./src/sass/lib/')); 
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) 
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});
gulp.task('svg:build', function () {
    gulp.src(path.src.svg) 
        .pipe(gulp.dest(path.build.svg))
        .pipe(reload({stream: true}));
});
gulp.task('video:build', function () {
    gulp.src(path.src.video) 
        .pipe(gulp.dest(path.build.video))
        .pipe(reload({stream: true}));
});
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});
// icon font
var fontname = 'svgfont';
gulp.task('font-svg:build', function(){
  return gulp.src(path.src.fontSvg)
    // .pipe(svgmin())
    .pipe(iconfont({
      fontName: fontname,
      prependUnicode: true,
      formats: ['ttf', 'eot', 'woff', 'woff2'],
      // timestamp: runTimestamp,
      normalize: true,
      fontHeight: 1001,
      fontStyle: 'normal',
      fontWeight: 'normal'
    }))
    .on('glyphs', function(glyphs, options) {
        console.log(glyphs);
        gulp.src('src/helpers/_svgfont.sass')
            .pipe(consolidate('lodash', {
                glyphs: glyphs,
                fontName: fontname,
                fontPath: '../fonts/',
                className: 'i'
            }))
            .pipe(gulp.dest('src/sass/'));
        gulp.src('src/helpers/icons.html')
            .pipe(consolidate('lodash', {
                glyphs: glyphs,
                fontName: fontname,
                fontPath: '../fonts/',
                className: 'i',
                htmlBefore: '<i class="i ',
                htmlAfter: '"></i>',
                htmlBr: ''
            }))
            .pipe(gulp.dest('build/'));
    })
    .pipe(gulp.dest('build/fonts/'))
    .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'sass:build',
    'sprite:build',
    'fonts:build',
    'image:build',
    'video:build',
    'svg:build',
    'font-svg:build'
]);


gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.sass], function(event, cb) {
        gulp.start('sass:build');
    });
    watch([path.watch.icons], function(event, cb) {
        gulp.start('sprite:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.svg], function(event, cb) {
        gulp.start('svg:build');
    }); 
    watch([path.watch.video], function(event, cb) {
        gulp.start('video:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch([path.watch.fontSvg], function(event, cb) {
        gulp.start('font-svg:build');
    });
});


gulp.task('default', ['build', 'webserver', 'watch']);