//= ../../bower_components/jquery/dist/jquery.min.js

//= ../../bower_components/jquery-ui/ui/minified/core.min.js
//= ../../bower_components/jquery-ui/ui/minified/widget.min.js
//= ../../bower_components/jquery-ui/ui/minified/mouse.min.js
//= ../../bower_components/jquery-ui/ui/minified/position.min.js
//= ../../bower_components/jquery-ui/ui/minified/slider.min.js

//= ../../bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.min.js



//= ../../bower_components/select2/dist/js/select2.full.js

//= ../../bower_components/slick-carousel/slick/slick.min.js

//= ../../bower_components/masonry/dist/masonry.pkgd.min.js

//= ../../bower_components/jquery-validation/dist/jquery.validate.min.js

//= ../../bower_components/raty/lib/jquery.raty.js

//= ../../bower_components/fancybox/source/jquery.fancybox.pack.js
//= ../../bower_components/fancybox/source/helpers/jquery.fancybox-thumbs.js
//= ../../bower_components/fancybox/source/helpers/jquery.fancybox-media.js


//= partials/head.js

//= partials/materialize/global.js
//= partials/materialize/buttons.js
//= partials/materialize/collapsible.js
//= partials/materialize/dropdown.js
//= partials/materialize/tabs.js
//= partials/materialize/transitions.js
//= partials/materialize/waves.js
//= partials/materialize/velocity.min.js
//= partials/materialize/tooltip.js
//= partials/perfect-scrollbar.jquery.js

//= app.js