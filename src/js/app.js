(function() {
	
	$(document).click(function(){
		if ($(".js-search").hasClass("is-active")) {
			$(".js-toggle-search").removeClass("is-active");
			$(".js-search").removeClass("is-active");
		}
		if ($(".js-dropdown").hasClass("is-active")) {
			$(".js-dropdown-trigger").removeClass("is-active");
			$(".js-dropdown").removeClass("is-active");
		}
		if ($(".js-card").hasClass("is-active")) {
			
			$(".js-card").removeClass("is-active");
		}
	});

// show/hide any blocks
	$(".js-toggle").on("click", function() {
		var el = $(this).attr("data-toggle");
		$("."+el).toggleClass("is-active");
		$(this).toggleClass("is-active");
		return false;
	});

// search
	$(".js-toggle-search").on("click", function(event) {
		$(this).parents(".js-search").toggleClass("is-active");
		$(this).parents(".js-search").find(".input").trigger("click");
		event.stopPropagation();
		return false;
	});
	$(".js-search .search-form__main").on("click", function(event) {
		event.stopPropagation();
	});

	$(".js-search .search__main").on("click", function(event) {
		event.stopPropagation();
	});

// dropdown
	$(".js-dropdown-trigger").on("click", function(event) {
		var dropdown = $(this).attr("data-dropdown");
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active");
			$("."+dropdown).removeClass("is-active");
		}
		else {
			$(".js-dropdown-trigger").removeClass("is-active");
			$(".js-dropdown").removeClass("is-active");
			$(this).addClass("is-active");
			$("."+dropdown).addClass("is-active");
		}
		event.stopPropagation();
		return false;
	});
	$(".js-dropdown").on("click", function(event) {
		event.stopPropagation();
	});

	$(".js-dropdown").on("click", function(event) {
		event.stopPropagation();
	});

// remove parent
	$(".js-del").on("click", function(event) {
		$(this).parents(".js-parent").remove();
		return false;
	});

// select 2 init
	$(".js-select").select2();

	$('.js-select-json').select2({
		minimumInputLength: 1,
		ajax: {
			type: 'post',
			url: 'url-to-your-json-file/file.json',
			dataType: 'json',
			minimumInputLength: 1,
			data: function (term, page) {
				return {
					q: term
				};
			},
			results: function (data, page) {
				return { results: data };
			}
		}
	});


// header extend
	$(".js-toggle-header").on("click", function() {
		$(".header").toggleClass("is-active");
		$(this).toggleClass("is-active");
		return false;
	});

// menu
	$(".js-toggle-menu").on("click", function() {
		$(".js-menu").toggleClass("is-active");
		$(this).toggleClass("is-active");
		return false;
	});

	$('.js-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-slider").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '80px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 400,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '50px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 375,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".js-prev").on("click", function() {
		$(this).parent().find(".slick-prev").trigger("click");
		return false;
	});

	$(".js-next").on("click", function() {
		$(this).parent().find(".slick-next").trigger("click");
		return false;
	});

	$(".js-pop-slider").slick({
		infinite: true,
		slidesToShow: 1,
		dots: false,
		arrows: true,
		centerMode: true,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 4000
	});

	$("body").on("click", ".js-card", function(event) {
		alert();
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active")
		}
		else {
			$(".js-card").removeClass("is-active");
			$(this).addClass("is-active");
		}
		event.stopPropagation();
	});

	$("body").on("click", ".slick-dots", function(event) {
		$(".js-el").removeClass("is-active");
		event.stopPropagation();
	});

	$(".js-el").on("click", function() {
		if ($(this).hasClass("is-active")) {
			$(this).removeClass("is-active")
		}
		else {
			$(".js-el").removeClass("is-active");
			$(this).addClass("is-active");
		}
		//event.stopPropagation();
	});
	
	$(".js-card").hover(function(){
		$(this).addClass("is-active");
	},
	function(){
		$(this).removeClass("is-active");
	});
	

	
// ----------------- //
	function fixHeader() {
		var top = $(".search").outerHeight();
		var scroll = $(document).scrollTop();
		var header = $(".js-header-index");
		if (scroll >= top) {
			header.removeClass("header_index").addClass("is-fixed");
		}
		else {
			header.addClass("header_index").removeClass("is-fixed");
		}
		
	}
	function fixSort() {
		var top = $(".page-head").offset().top + $(".page-head").outerHeight();
		var scroll = $(document).scrollTop();
		var header = $(".js-header-index").outerHeight();
		if (scroll + header >= top) {
			$("body").addClass("is-fixed-sort");
		}
		else {
			$("body").removeClass("is-fixed-sort");
		}
		
	}
	if ($(".js-header-index").length) {
		fixHeader();
	}
	if ($(".js-sort-wrap").length) {
		fixSort()
	}
	$(window).resize(function() {
		if ($(".js-header-index").length) {
			fixHeader();
		}
		if ($(".js-sort-wrap").length) {
			fixSort()
		}
	});
	$(window).scroll(function() {
		if ($(".js-header-index").length) {
			fixHeader();
		}
		if ($(".js-sort-wrap").length) {
			fixSort()
		}
	});

	$('.js-masonry').masonry({
		// options
		itemSelector: '.js-masonry-item',
		stamp: '.js-masonry-stamp',
		columnWidth: 314,
		gutter: 40
	});

	$('.js-masonry-on-resize').masonry({
		// options
		itemSelector: '.js-masonry-item',
		columnWidth: 280,
		gutter: 40
	});

	function validate() {
		$('.js-validate').each(function(){
			if ($(this).length > 0) {
				$(this).validate({
					errorClass: 'has-error',
					rules: {
						username: {
							minlength: 2
						},
						any: {
							minlength: 2
						},
						password: {
							minlength: 5
						},
						confirm_password: {
							minlength: 5,
							equalTo: '#password'
						},
						email: {
							email: true
						},
						tel: {
							minlength: 2,
						},
						address: {
							minlength: 2
						},
						message: {
							minlength: 4
						},
						field: {
							required: true
						},
						// fruit: {
						//   required: true
						// }
					},
					messages: {
						firstname: 'Вас так зовут?',
						lastname: 'У вас такая фамилия?',
						fathername: 'У вас такое отчество?',
						password: {
							required: 'Введите пароль',
							minlength: 'Минимум 5 символов'
						},
						confirm_password: {
							 required: 'Пароли не совпадают',
							 minlength: 'Минимум 5 символов',
							 equalTo: 'Пароли не совпадают'
						},
						email: 'Неверный формат',
						address: 'Это Ваш адрес?',
						any: 'Заполните поле',
						company: 'Заполните поле',
						tel: {
							required: 'Заполните поле',
						},
						message: {
							required: 'Заполните поле',
							minlength: 'Заполните поле'
						}
					}
				});
			}
		});
	}
		
	validate();

	$('.js-tel').on('keyup', function(){
		var value = $(this).val();
		var re = /[^0-9,]/;
		if (re.test(value)) {
			value = value.replace(re, '');
			$(this).val(value);
		}
			// set max and min value
		if($(this).val().length < 7 || $(this).val().length > 12) {
			$(this).addClass('has-error');
		} else {
			$(this).removeClass('has-error');
		}
	});

	$('.js-promo-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-promo-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-promo-slider").slick({
		infinite: true,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		autoplay: true,
		adaptiveHeight: true,
		autoplaySpeed: 4000
	});

	$('.js-top-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-top-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-top-slider").slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 740,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 400,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '20px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 375,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '15px',
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$(".js-el-slider").slick({
		infinite: false,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		draggable: false,
		//lazyLoad: 'ondemand',
		swipe: false
	});
	$(".js-el-slider-touch").slick({
		infinite: true,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		//lazyLoad: 'ondemand',
	});
	
	$(".js-cat-slider").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		dots: false,
		arrows: false,
		//centerMode: true,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 1000,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					autoplay: false,
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});


// sort slider top
	$('.js-cat-slider a').on('click', function(){
	var el = $(this).attr("data-filter");
	  if (!$(this).hasClass("is-active")) {
		$('.js-cat-slider a').removeClass("is-active");
		$(this).addClass("is-active");
		//$('.js-top-slider').slick('slickFilter','.'+el);
		//filtered = true;
	  } else {
		$(this).removeClass("is-active");
		//$('.js-top-slider').slick('slickUnfilter');
		//filtered = false;
	  }
	  return false;
	});

	$('.js-sort-slider a').on('click', function(){
	  var el = $(this).attr("data-filter");
	  if (!$(this).parent().hasClass("is-active")) {
		$('.js-sort-slider li').removeClass("is-active");
		$(this).parent().addClass("is-active");
		//$('.js-slider-hot').slick('slickFilter','.'+el);
	  } else {
		$(this).removeClass("is-active");
		//$('.js-slider-hot').slick('slickUnfilter');
	  }
	  return false;
	});

	$(".js-search-example a").on('click', function(){
		var value = $(this).attr("data-val");
		$(".js-select-band .js-select").val(value);
		$(".js-select-band .select2-selection__placeholder").text(value).addClass("is-active");
		$(".js-select-band .select2-selection__rendered").text(value);
		return false;
	});

	$(".js-popup-trigger").on('click', function(){
		var popup = $(this).attr("data-popup");
		$("."+popup).fadeIn();
		return false;
	});

	$(".js-popup-close").on('click', function(){
		$(this).parents(".js-popup").fadeOut();
		return false;
	});
	$('.js-article-slider').on('init', function(slick) {
		setTimeout(function(){
			$('.js-article-slider').parent().addClass("is-ready");
		},200);
	});
	$(".js-article-slider").slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1
	});
	$(".js-raty").raty({
		score: function() {
			return $(this).attr('data-score');
		},
		hints: ['1', '2', '3', '4', '5'],
		path: 'img/svg',
		starOn: 'star-act.svg',
		starOff: 'star.svg',
	});
	$('.js-raty-readonly').raty({
		readOnly: true,
		score: function() {
			return $(this).attr('data-score');
		},
		hints: ['1', '2', '3', '4', '5'],
		path:      'img/svg',
		starOn:    'star-act.svg',
		starOff:   'star.svg',
		half: true,
		starHalf: 'star-half.svg'
	});

	$('.js-carousel').on('init', function(slick) {
		setTimeout(function(){
			$('.js-carousel').parent().addClass("is-ready");
		},200);
	});

	 $('.js-carousel').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: true,
	  dots: true,
	  adaptiveHeight: true,
	  asNavFor: '.js-carousel-nav'
	});
	$('.js-carousel-nav').slick({
	  slidesToShow: 9,
	  slidesToScroll: 1,
	  asNavFor: '.js-carousel',
	  dots: false,
	  arrows: true,
	  adaptiveHeight: true,
	  centerMode: false,
	  focusOnSelect: true,
	  responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 5,
				}
			}
		]
	});
	$(".js-fancybox").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		padding: 50,
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 108,
				height	: 108
			}
		}
	});

	$('.js-fancybox-media').fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		padding: 0,
		helpers	: {
			media: {},
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 108,
				height	: 108
			}
		},
		afterLoad: function(current, previous) {
			console.info( 'Current: ' + current.href );        
			console.info( 'Previous: ' + (previous ? previous.href : '-') );
			
			if (previous) {
				console.info( 'Navigating: ' + (current.index > previous.index ? 'right' : 'left') );     
			}
		}
	});

	$(".js-fancybox-trigger").on('click', function(){
		$(this).parent().children().first().find("a").trigger("click");
		return false;
	});

	function uiSlider() {
		$(".js-ui-slider").each(function(){
			var slider = $(this).find(".js-ui-slider-main"),
				inputFrom = $(this).find(".js-ui-slider-from"),
				inputFromHidden = $(this).find(".js-input-from-hidden"),
				inputTo = $(this).find(".js-ui-slider-to"),
				inputToHidden = $(this).find(".js-input-to-hidden"),
				maxVal = +slider.attr("data-max"),
				minVal = +slider.attr("data-min"),
				valFrom = inputFromHidden.val(),
				valTo = inputToHidden.val(),
				stepVal = +slider.attr("data-step");
				if (!valFrom) {
					var valFrom = minVal;
				}
				if (!valTo) {
					var valTo = maxVal;
				}
			slider.slider({
				range: true,
				min: minVal,
				max: maxVal,
				step: stepVal,
				values: [ valFrom, valTo ],
				slide: function( event, ui ) {
					$(this).find(".ui-slider-handle").html("<span></span>");
					var handle_0 = $(this).find(".ui-slider-range").next().find("span")
					var handle_1 = $(this).find(".ui-slider-range").next().next().find("span");
					//inputFrom.val(ui.values[0]);
					inputFromHidden.val(ui.values[0]);
					//inputTo.val(ui.values[1]);
					inputToHidden.val(ui.values[1]);
					handle_0.text(ui.values[0]+" €");
					handle_1.text(ui.values[1]+" €");
					$(".js-sort-form .js-btn-reset").removeAttr("disabled");
				}
			});
			//console.log(handle_0);
			//console.log(handle_1);
			$(this).find(".ui-slider-handle").html("<span></span>");
			var handle_0 = $(this).find(".ui-slider-range").next().find("span")
			var handle_1 = $(this).find(".ui-slider-range").next().next().find("span");
			handle_0.text(slider.slider( "values", 0) + " €");
			handle_1.text(slider.slider( "values", 1) + " €");
			inputFromHidden.val(slider.slider( "values", 0));
			inputToHidden.val(slider.slider( "values", 1));
		});
	}
	uiSlider();

	$(".js-sort-form input").on("change", function() {
		if ($(this).parents(".js-sort-form").find("input:checked").length > 0) {

			$(".js-sort-form .js-btn-reset").removeAttr("disabled");
		}
	});
	$(".js-btn-reset").on("click", function() {
		var min = $(".js-ui-slider-main").attr("data-min");
		var max = $(".js-ui-slider-main").attr("data-max");
		var slider = $(".js-ui-slider-main");
		$(this).attr("disabled", "disabled");
		slider.slider( "values", [ min, max ] );
		slider.find(".ui-slider-range").next().find("span").text(min+" €");
		slider.find(".ui-slider-range").next().next().find("span").text(max+" €");
		$(".js-sort-form")[0].reset();
	});

	$(".js-manual-slider").slick({
		infinite: true,
		fade: true,
		speed: 200,
		slidesToShow: 1,
		dots: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000
	});

	if ($('.js-scrollbar').length > 0) {
		$('.js-scrollbar').perfectScrollbar();
	}

})();