You have to install [Bower](http://bower.io/) and [Gulp](http://gulpjs.com/).

Install local gulp and bower

1. *npm install* gulp

2. *npm install bower*

Install plugins

1. *npm install* (plugins for gulp)

2. *bower install* (jquery plugins)